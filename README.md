# LobbyGames

A plugin to facilitate interaction between minigames within the same world and provide common functionality such as 
giving players items.

The plugin works on the concept of being able to start and end minigames through events. These events not only allow
coordination between minigame plugins but also cause this plugin to give or take clickable items from players.

## Usage

Simply broadcast a `GameStartedEvent` whenever a player started playing your minigame and broadcast a `GameLeftEvent` whenever a player stopped playing your minigame. You can listen for these events and act accordingly if you desire. See the Javadoc for the details!
