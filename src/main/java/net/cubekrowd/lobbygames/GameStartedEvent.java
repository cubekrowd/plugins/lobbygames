package net.cubekrowd.lobbygames;

import java.util.Collections;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Singular;
import lombok.ToString;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Broadcast this event just after a player has started playing your minigame.
 * You must always broadcast a matching {@link GameLeftEvent} after broadcasting
 * a {@link GameStartedEvent}; even if the player is leaving the server while
 * playing the minigame. You must not broadcast a second
 * {@link GameStartedEvent} for the same minigame and player before sending the
 * matching {@link GameLeftEvent}.
 *
 * Other minigame plugins can listen for this event so minigames can be ended if
 * a new one is started.
 *
 * When called all the player's clickable items will be removed and replaced
 * with the ones specified in this event.
 */
@Builder
@ToString
@RequiredArgsConstructor
public class GameStartedEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    /**
     * The player who is starting the minigame.
     */
    @Getter
    @NonNull
    private final Player player;

    /**
     * The minigame which is being started.
     */
    @Getter
    @NonNull
    private final Minigame minigame;

    /**
     * Items which will be added to the available slots of the player when this event is called.
     */
    @Getter
    @Singular
    private final List<ClickableItem> items;

    public GameStartedEvent(Player player, Minigame minigame) {
        this(player, minigame, Collections.emptyList());
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
