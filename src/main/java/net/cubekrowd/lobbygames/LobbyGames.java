package net.cubekrowd.lobbygames;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * The LobbyGames plugin, see {@link GameStartedEvent} and {@link GameLeftEvent}
 * for usage.
 */
public class LobbyGames extends JavaPlugin implements Listener {

    public static LobbyGames INSTANCE;
    private Map<Player, List<GameStartedEvent>> playersInMinigames = new HashMap<>();

    @Override
    public void onEnable() {
        INSTANCE = this;
        Bukkit.getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
        INSTANCE = null;
    }

    public void addItemsForStartedEvent(GameStartedEvent e) {
        var player = e.getPlayer();
        var inventory = player.getInventory();

        for (var item : e.getItems()) {
            boolean added = inventory.addItem(item.createItemStack(e.getMinigame())).isEmpty();
            if (!added) {
                LobbyGames.INSTANCE.getLogger().warning("Failed to add item " + item + " to " + player);
                break;
            }
        }
    }

    public List<Minigame> getActiveGames(Player player) {
        var startEvents = playersInMinigames.get(player);
        if (startEvents == null || startEvents.isEmpty()) {
            return Collections.emptyList();
        }
        return startEvents.stream().map(e -> e.getMinigame()).collect(Collectors.toList());
    }

    @EventHandler
    private void onGameStarted(GameStartedEvent e) {
        var player = e.getPlayer();
        var list = playersInMinigames.computeIfAbsent(player, p -> new ArrayList<>());
        list.add(e);
        addItemsForStartedEvent(e);
    }

    @EventHandler
    private void onGameLeft(GameLeftEvent e) {
        var player = e.getPlayer();
        // assumed to be non null and non empty
        var startEvents = playersInMinigames.get(player);

        for (int i = 0; i < startEvents.size(); i++) {
            var startEvent = startEvents.get(i);
            if (startEvent.getMinigame().getName().equals(e.getMinigame().getName())) {
                // remove items for minigame that the player just left
                if (ClickableItem.clear(player, e.getMinigame()) > 0) {
                    // move the items of subsequent minigames back a few slots.
                    // Don't remove items and re-add them when we don't have to,
                    // to prevent the client from seeing an item-add animation.

                    for (int j = i + 1; j < startEvents.size(); j++) {
                        ClickableItem.clear(player, startEvents.get(j).getMinigame());
                    }
                    for (int j = i + 1; j < startEvents.size(); j++) {
                        addItemsForStartedEvent(startEvents.get(j));
                    }
                }

                startEvents.remove(i);
                if (startEvents.isEmpty()) {
                    playersInMinigames.remove(player);
                }
                break;
            }
        }
    }

    @EventHandler
    private void onInteract(PlayerInteractEvent e) {
        ItemStack stack = e.getItem();
        String actionId = ClickableItem.getActionId(stack);

        if (actionId != null) {
            Bukkit.getPluginManager().callEvent(
                    new ClickableItemUseEvent(e.getPlayer(), actionId)
            );
        }
    }

    // @NOTE(traks) in case Spigot decided to not fire events when it's supposed
    // to or something else is being buggy, print a warning and attempt to clean
    // up.
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerQuit(PlayerQuitEvent e) {
        var player = e.getPlayer();
        var list = playersInMinigames.get(player);
        if (list != null) {
            getLogger().warning("Player " + player.getName()
                    + " is still playing games on quit: " + list
                    + ". Cleaning player up as a safety measure.");

            playersInMinigames.remove(player);
            ClickableItem.clear(player);
        }
    }
}
