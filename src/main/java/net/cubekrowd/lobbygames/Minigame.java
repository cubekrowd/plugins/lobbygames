package net.cubekrowd.lobbygames;

/**
 * Describes a minigame and optionally holds data about it.
 */
public interface Minigame {

    /**
     * Provides a globally unique identifier for the minigame.
     */
    String getName();
}
