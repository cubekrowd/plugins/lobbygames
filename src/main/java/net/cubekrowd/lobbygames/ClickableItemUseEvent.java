package net.cubekrowd.lobbygames;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Invoked whenever a clickable item is left or right clicked by a player.
 */
@AllArgsConstructor
public class ClickableItemUseEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    /**
     * The player who clicked the item.
     */
    @Getter
    private final Player player;

    /**
     * The action ID of the item as specified in {@link ClickableItem}.
     */
    @Getter
    private final String actionId;

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
