package net.cubekrowd.lobbygames;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Broadcast this event just after a player has stopped playing your minigame.
 * You must always broadcast a matching {@link GameLeftEvent} after broadcasting
 * a {@link GameStartedEvent}; even if the player is leaving the server while
 * playing the minigame.
 *
 * Other plugins may listen for this event but it should not be assumed that
 * calling this will end other minigames.
 *
 * When called and the {@link Minigame} matches the active minigame of the
 * player, all clickable items will be removed from their inventory.
 */
@Builder
@ToString
@RequiredArgsConstructor
public class GameLeftEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    /**
     * The player who is leaving the minigame.
     */
    @Getter
    @NonNull
    private final Player player;

    /**
     * The minigame the player is leaving.
     */
    @Getter
    @NonNull
    private final Minigame minigame;

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
