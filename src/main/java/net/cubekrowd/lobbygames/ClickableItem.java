package net.cubekrowd.lobbygames;

import lombok.Builder;
import lombok.NonNull;
import lombok.ToString;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

/**
 * A schematic for an item that players can use to perform actions when playing a minigame.
 */
@Builder
@ToString
public class ClickableItem {

    /**
     * The material of the item.
     */
    @NonNull
    private final Material material;

    /**
     * The action ID which can be later used to distinguish {@link ClickableItemUseEvent}s.
     *
     * Should be globally unique.
     */
    @NonNull
    private final String actionId;

    /**
     * Custom name for the item given to the player, may contain formatting codes.
     */
    private final String name;

    public ItemStack createItemStack(Minigame minigame) {
        ItemStack stack = new ItemStack(material);
        ItemMeta meta = stack.getItemMeta();

        meta.getPersistentDataContainer().set(
                new NamespacedKey(LobbyGames.INSTANCE, "action-id"),
                PersistentDataType.STRING,
                actionId
        );
        meta.getPersistentDataContainer().set(
                new NamespacedKey(LobbyGames.INSTANCE, "minigame"),
                PersistentDataType.STRING,
                minigame.getName()
        );

        if (name != null) {
            meta.setDisplayName(ChatColor.RESET + name);
        }

        stack.setItemMeta(meta);
        return stack;
    }

    /**
     * Queries for the action ID associated with an item.
     *
     * Returns null if the item was not generated from this class or if null was passed.
     */
    public static String getActionId(ItemStack stack) {
        if (stack == null) {
            return null;
        }

        ItemMeta meta = stack.getItemMeta();
        return meta.getPersistentDataContainer().get(
                new NamespacedKey(LobbyGames.INSTANCE, "action-id"),
                PersistentDataType.STRING
        );
    }

    public static String getMinigame(ItemStack stack) {
        if (stack == null) {
            return null;
        }

        ItemMeta meta = stack.getItemMeta();
        return meta.getPersistentDataContainer().get(
                new NamespacedKey(LobbyGames.INSTANCE, "minigame"),
                PersistentDataType.STRING
        );
    }

    public static void clear(Player player) {
        PlayerInventory inventory = player.getInventory();
        ItemStack[] items = inventory.getContents();

        for (int i = 0; i < items.length; i++) {
            if (ClickableItem.getActionId(items[i]) != null) {
                items[i] = null;
            }
        }

        inventory.setContents(items);
    }

    public static int clear(Player player, Minigame minigame) {
        var inventory = player.getInventory();
        var items = inventory.getContents();
        int removed = 0;

        for (int i = 0; i < items.length; i++) {
            if (getActionId(items[i]) != null
                    && minigame.getName().equals(getMinigame(items[i]))) {
                items[i] = null;
                removed++;
            }
        }

        inventory.setContents(items);
        return removed;
    }
}
